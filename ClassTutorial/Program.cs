﻿using System;

namespace ClassTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("How many rectangles: ");
            string numrectangles = Console.ReadLine();
            Program.rTest(Convert.ToInt32(numrectangles));

            Console.Write("How many circles: ");
            string nCircles = Console.ReadLine();
            Program.circleTest(Convert.ToInt32(Convert.ToInt32(nCircles)));

            Console.ReadLine();
        }

        static void rTest(int count)
        {
            //Rectangle[] rArr = { new Rectangle(11, 24), new Rectangle(23, 92), new Rectangle(88, 239458), new Rectangle(2, 2) };
            Rectangle[] rArr = new Rectangle[count];

            for(uint i = 1; i<=count; i++)
            {
                rArr[i - 1] = new Rectangle(i * 5, i * 5);
            }


            Console.WriteLine();
            Console.WriteLine("There are " + rArr.Length + " rectangles.");
            foreach (Rectangle element in rArr)
            {
                element.printAttributes();
            }
        }

        static void circleTest(int count)
        {
            //Circle[] cArr = { new Circle(5), new Circle(10), new Circle(15), new Circle(20), new Circle(25), new Circle(Math.PI) };
            Circle[] circles = new Circle[count];

            for(int i = 1; i<=count; i++)
            {
                circles[i-1] = new Circle(i * 5);
            }

            Console.WriteLine();
            Console.WriteLine("There are " + circles.Length + " circles.");
            Console.WriteLine("There are " + circles.Length + " circles.");
            foreach (Circle element in circles)
            {
                element.printAttributes();
            }
            Console.ReadLine();
        }
    }

    class Rectangle
    {
        private uint x;
        private uint y;


        public Rectangle(uint x, uint y)
        {
            Console.WriteLine("Constructing rectangle with dimensions (" + x + ", " + y + ")");
            this.x = x;
            this.y = y;
        }

        public uint getArea()
        {
            return this.x * this.y;
        }

        public uint getPerimeter()
        {
            return this.x + this.y;
        }

        public double getDiagonalLength()
        {
            double diagonalLength = Math.Sqrt(Math.Pow(x,2) + Math.Pow(y,2));
            return diagonalLength;
        }

        public void printAttributes()
        {
            Console.WriteLine();
            Console.WriteLine("Base: " + this.x);
            Console.WriteLine("Height: " + this.y);
            Console.WriteLine("Area: " + this.getArea());
            Console.WriteLine("Perimeter: " + this.getPerimeter());
            Console.WriteLine("Diagonal Length: " + Math.Round(this.getDiagonalLength(), 2));
        }


    }

    class Circle
    {
        private double radius;
        private double diameter;

        public Circle(double radius)
        {
            Console.WriteLine("Constructing circle with radius " + Math.Round(radius,4) + " and diameter " + Math.Round(radius * 2,4)  + ".");
            this.radius = radius;
            diameter = 2 * radius;
        }

        public double Circumference
        {
            get { return Math.Round((2 * Math.PI * this.radius), 4); }
        }

        public double Area
        {
            get { return Math.PI * Math.Pow(this.radius, 2); }
        }

   

        public void printAttributes()
        {
            Console.WriteLine();
            Console.WriteLine("Radius: " + Math.Round(this.radius, 4));
            Console.WriteLine("Diameter: " + Math.Round(this.diameter, 4));
            Console.WriteLine("Circumference: " + Math.Round(this.Circumference,4));
        }
    }
}
